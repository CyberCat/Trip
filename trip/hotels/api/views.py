from rest_framework import generics, status
from rest_framework.response import Response
from .serializers import (
  HotelSerializers, RentHotelSerializers
)
from ..models import Hotel, RentHotel

class HotelView(generics.ListAPIView):
  permission_classes = []
  model = Hotel
  serializer_class = HotelSerializers

  def get_queryset(self):
    return self.model.objects.all()

'''
class RentHotelView(generics.ListAPIView):
  permission_classes = []
  model = RentActivity
  serializer_class = RentActivitySerializers
  
  def get_queryset(self):
    return self.model.objects.all()
'''
