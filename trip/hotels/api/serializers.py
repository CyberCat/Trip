from ..models import Hotel, RentHotel
from rest_framework import serializers

class HotelSerializers(serializers.ModelSerializer):
  class Meta:
    model = Hotel
    fields = '__all__'

class RentHotelSerializers(serializers.ModelSerializer):
  class Meta:
    model = RentHotel
    fields = '__all__'
