from django.conf.urls import url
from . import views

app_name = 'api_inventory'

urlpatterns = [
  url(r'^hotellist', views.HotelView.as_view(), name="activitylist"),
]