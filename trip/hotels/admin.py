from django.contrib import admin
from .models import Hotel, RentHotel

class HotelAdmin(admin.ModelAdmin):
	list_display=[f.name for f in Hotel._meta.fields]
admin.site.register(Hotel, HotelAdmin)

class RentHotelAdmin(admin.ModelAdmin):
	list_display=[f.name for f in RentHotel._meta.fields]
admin.site.register(RentHotel, RentHotelAdmin)
