from django.db import models
from rents.models import Rent

class Hotel(models.Model):
  SIGLE_BED = '1'
  TWO_BED = '2'
  KING_SIZE_BED = 'K'
  TRIPLE = 'T' 
  ROOM_TYPE_CHOICES = (
    (SIGLE_BED, 'Single Bed'),
    (TWO_BED, 'Two Bed'),
    (KING_SIZE_BED, 'King Size'),
    (TRIPLE, 'Triple'),
  )
  name = name = models.CharField(max_length=50, blank=False, db_index=True)
  location = models.TextField(blank=True) #ยังไม่แน่ใจว่าดึงจากกูเกิ้ลได้ไหมเอาไทป์นี้ไปก่อน
  info = models.TextField(blank=True)
  room_type = models.CharField(max_length=1, choices=ROOM_TYPE_CHOICES)
  room_left = models.PositiveIntegerField(blank=False, default=0)
  image = models.ImageField(upload_to='hotelpicture/',default='hotelpicture/default.jpg')
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

class RentHotel(models.Model):
  rent = models.ForeignKey(Rent, on_delete=models.CASCADE, blank=False)
  hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, blank=False)
  start_date = models.DateTimeField(auto_now_add=True, blank=True)
  end_date = models.DateTimeField(auto_now_add=True, blank=True)
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
  review = models.TextField(blank=True)
  score = models.DecimalField(max_digits=2, decimal_places=1, default=0.0)