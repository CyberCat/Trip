from django.db import models
from rents.models import Rent

class Car(models.Model):
  TAXI_TYPE = 'T'
  VAN_TYPE = 'V'
  CAR_TYPE_CHOICES = (
    (TAXI_TYPE, 'Taxi'),
    (VAN_TYPE, 'Van'),
  )
  car_number = models.CharField(max_length=10, blank=False, db_index=True)
  car_name = models.CharField(max_length=25, blank=False, db_index=True)
  car_info = models.TextField(blank=True)
  driver_id = models.CharField(max_length=10, blank=False, db_index=True)
  driver_name = models.CharField(max_length=30, blank=False, db_index=True)
  driver_info = models.TextField(blank=True)
  car_type = models.CharField(max_length=1, choices=CAR_TYPE_CHOICES)
  service_area = models.TextField(blank=True)
  image = models.ImageField(upload_to='carpicture/',default='carpicture/default.jpg')
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

class RentTransport(models.Model):
  rent = models.ForeignKey(Rent, on_delete=models.CASCADE, blank=False)
  car = models.ForeignKey(Car, on_delete=models.CASCADE, blank=False)
  start_date = models.DateTimeField(blank=True)
  end_date = models.DateTimeField(blank=True)
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
  review = models.TextField(blank=True)
  score = models.DecimalField(max_digits=2, decimal_places=1, default=0.0)

  
