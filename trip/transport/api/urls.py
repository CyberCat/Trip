from django.conf.urls import url
from . import views

app_name = 'api_inventory'

urlpatterns = [
  url(r'^carlist', views.CarView.as_view(), name="activitylist"),
  url(r'^rentcarlist', views.RentCarView.as_view(), name="activitylist"),
]