from ..models import Car, RentTransport
from rest_framework import serializers

class CarSerializers(serializers.ModelSerializer):
  class Meta:
    model = Car
    fields = '__all__'

class RentCarSerializers(serializers.ModelSerializer):
  class Meta:
    model = RentTransport
    fields = '__all__'
