from rest_framework import generics, status
from rest_framework.response import Response
from .serializers import (
  CarSerializers, RentCarSerializers
)
from ..models import Car, RentTransport

class CarView(generics.ListAPIView):
  permission_classes = []
  model = Car
  serializer_class = CarSerializers

  def get_queryset(self):
    return self.model.objects.all()


class RentCarView(generics.ListAPIView):
  permission_classes = []
  model = RentTransport
  serializer_class = RentCarSerializers
  
  def get_queryset(self):
    return self.model.objects.all()
