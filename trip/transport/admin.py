from django.contrib import admin
from .models import Car, RentTransport

class CarAdmin(admin.ModelAdmin):
	list_display=[f.name for f in Car._meta.fields]
admin.site.register(Car, CarAdmin)

class RentTransportAdmin(admin.ModelAdmin):
	list_display=[f.name for f in RentTransport._meta.fields]
admin.site.register(RentTransport, RentTransportAdmin)