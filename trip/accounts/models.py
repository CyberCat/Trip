# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser
from jsonfield import JSONField


class User(AbstractUser):

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

class PaymentMethod(models.Model):
    CREDIT_TYPE = 'C'
    DEBIT_TYPE = 'D'
    PAYPAL_TYPE = 'P'
    PAYMENT_TYPE_CHOICES = (
        (CREDIT_TYPE, 'Credit'),
        (DEBIT_TYPE, 'Debit'),
        (PAYPAL_TYPE, 'Paypal'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    payment_type = models.CharField(max_length=1, choices=PAYMENT_TYPE_CHOICES)
    info = JSONField()#if DB ENGINE NOT SUPPORT use TextField(blank=True)