# -*- coding: utf-8 -*-
#pylint: disable=E0602,C0103,C0326,W

import requests
from django.contrib import messages
from django.contrib.auth import (authenticate, login, logout,
                                 update_session_auth_hash)
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from trip import settings

from .forms import UserForm


def use_sso():
    SSO_URL = getattr(settings, 'SSO_URL', None)
    return bool(SSO_URL)

def can_register():
    return bool(getattr(settings, 'CAN_REGISTER', False))


def parse_sso(user_json):
    username     = user_json['username']
    is_staff     = (user_json['level'] >= 90)
    is_superuser = (user_json['level'] >= 99)
    user         = User.objects.filter(username=username).first()
    if not user:
        user = User(username=username)
    user.first_name   = user_json['nickname']
    user.is_superuser = is_superuser
    user.is_staff     = is_staff
    user.save()
    return user

def register(request):
    if use_sso() or not can_register():
        return render(request, 'register_disable.html')

    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form_data = form.save(commit=False)
            user = User.objects.create_user(username=form_data.username, password=form_data.password)
            user = authenticate(username=form_data.username, password=form_data.password)
            login(request, user)
            next_url = request.GET.get('next', '/')
            return HttpResponseRedirect(next_url)
    else:
        form = UserForm()
    data = {
        'form': form,
    }
    return render(request, 'register.html', data)

@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            user.is_force_change_password = False
            user.save()
            messages.success(request, 'เปลี่ยนรหัสผ่านเสร็ตสิ้น!')
            # return redirect('change_password')
            return redirect('/')
        else:
            messages.error(request, 'พบปัญหา.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'accounts/change_password.html', {
        'form': form
    })
