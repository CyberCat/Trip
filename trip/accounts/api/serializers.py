
from rest_framework import serializers

from rest_framework import authentication
from ..models import User

class UserSerializer(serializers.ModelSerializer):

    authentication_classes = (authentication.TokenAuthentication,)

    class Meta:
        model = User
        fields = ('username', 'email', 'is_staff')
