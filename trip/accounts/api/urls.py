from django.conf.urls import include, url
from django.views.generic import RedirectView, TemplateView
from django.contrib.auth.views import logout
from . import views

app_name = "api_accounts"

urlpatterns = [
    url('^me$', views.CurrentUserView.as_view(), name="me"),
]
