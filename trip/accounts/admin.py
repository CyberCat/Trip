from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib import admin
from django import forms

from .models import User, PaymentMethod


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    pass

class PaymentMethodAdmin(admin.ModelAdmin):
	list_display=[f.name for f in PaymentMethod._meta.fields]
admin.site.register(PaymentMethod, PaymentMethodAdmin)