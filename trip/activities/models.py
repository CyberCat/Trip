from django.db import models
from rents.models import Rent

class Activity(models.Model):
  PER_DAY = 'D'
  PER_ROUND = 'R'
  PER_HOUR = 'H'
  PRICE_TYPE_CHOICES = (
    (PER_DAY, 'Per Day'),
    (PER_ROUND, 'Per Round'),
    (PER_HOUR, 'Per Hour'),
  )
  name = models.CharField(max_length=30, blank=False, db_index=True)
  location = models.TextField(blank=True) #ยังไม่แน่ใจว่าดึงจากกูเกิ้ลได้ไหมเอาไทป์นี้ไปก่อน
  info = models.TextField(blank=True)
  image = models.ImageField(upload_to='activitypicture/',default='activitypicture/default.jpg')
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
  price_type = models.CharField(max_length=1, choices=PRICE_TYPE_CHOICES)

class RentActivity(models.Model):
  rent = models.ForeignKey(Rent, on_delete=models.CASCADE, blank=False)
  activity = models.ForeignKey(Activity, on_delete=models.CASCADE, blank=False)
  start_date = models.DateTimeField(auto_now_add=True, blank=True)
  end_date = models.DateTimeField(auto_now_add=True, blank=True)
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
  review = models.TextField(blank=True)
  score = models.DecimalField(max_digits=2, decimal_places=1, default=0.0)
