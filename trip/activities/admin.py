from django.contrib import admin
from .models import Activity, RentActivity

class ActivityAdmin(admin.ModelAdmin):
	list_display=[f.name for f in Activity._meta.fields]
admin.site.register(Activity, ActivityAdmin)

class RentActivityAdmin(admin.ModelAdmin):
	list_display=[f.name for f in RentActivity._meta.fields]
admin.site.register(RentActivity, RentActivityAdmin)
