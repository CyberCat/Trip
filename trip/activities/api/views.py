from rest_framework import generics, status
from rest_framework.response import Response
from .serializers import (
  ActivitySerializers, RentActivitySerializers
)
from ..models import Activity, RentActivity

class ActivityView(generics.ListAPIView):
  permission_classes = []
  model = Activity
  serializer_class = ActivitySerializers

  def get_queryset(self):
    return self.model.objects.all()

'''
class RentActivityView(generics.ListAPIView):
  permission_classes = []
  model = RentActivity
  serializer_class = RentActivitySerializers
  
  def get_queryset(self):
    return self.model.objects.all()
'''
