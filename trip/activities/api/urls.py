from django.conf.urls import url
from . import views

app_name = 'api_inventory'

urlpatterns = [
  url(r'^activitylist', views.ActivityView.as_view(), name="activitylist"),
]