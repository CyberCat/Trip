from ..models import Activity, RentActivity
from rest_framework import serializers

class ActivitySerializers(serializers.ModelSerializer):
  class Meta:
    model = Activity
    fields = '__all__'

class RentActivitySerializers(serializers.ModelSerializer):
  class Meta:
    model = RentActivity
    fields = '__all__'
