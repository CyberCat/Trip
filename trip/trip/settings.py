import datetime
from .common_settings import *

INSTALLED_APPS += [
    'rest_framework',
    'django_filters',
    'corsheaders',
    'rents',
    'accounts',
    'activities',
    'hotels',
    'transport',
    'django_extensions',
]

AUTH_USER_MODEL = 'accounts.User'

BRAND_NAME = 'Trip'

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated',),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        #'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        #'rest_framework.authentication.SessionAuthentication',  # Session for debug
        # 'rest_framework.authentication.BasicAuthentication',
    ),
}

JWT_AUTH = { 
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=30, seconds=666),
}

MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True