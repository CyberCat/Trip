"""trip URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token
from django.contrib.staticfiles.urls import static
from .settings import DEBUG, MEDIA_URL, MEDIA_ROOT

api_urlpatterns = [
    url(r'auth/', obtain_jwt_token),
    url(r'api-auth/', include('rest_framework.urls', namespace="api_auth")),
    url(r'accounts/', include('accounts.api.urls', namespace="accounts")),
    url(r'hotels/', include('hotels.api.urls', namespace="hotels")),
    url(r'activities/', include('activities.api.urls', namespace="activities")),
    url(r'transport/', include('transport.api.urls', namespace="transport")),
    url(r'rents/', include('rents.api.urls', namespace="rents")),
]

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(api_urlpatterns, namespace='api')),
]
urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)