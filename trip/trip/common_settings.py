"""
Django settings for trip project.

Generated by 'django-admin startproject' using Django 1.11.9.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

from pathlib import Path
from .misc import import_app

class EnvironDict(object):

    def __getattr__(self, attrname):
        import os
        if attrname in os.environ:
            return os.environ[attrname]
        raise AttributeError()


try:
    import configs
except ImportError:
    try:
        configs = EnvironDict()
    except:
        configs = {}

BASE = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = getattr(configs, 'SECRET_KEY', '8u8^!z-k6nz5(9z6&b)4df%^9t88*hynzs!pr2#jw6=n^-e#az')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = getattr(configs, 'ALLOWED_HOSTS', ['192.168.1.100','localhost', '171.7.183.238', '172.25.75.92'])


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'trip.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [str(BASE / 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                'trip.context.brand',
            ],
        },
    },
]

if DEBUG:
    if import_app('debug_toolbar', INSTALLED_APPS):
        INTERNAL_IPS = ['127.0.0.1']
        MIDDLEWARE.insert(
            MIDDLEWARE.index('django.middleware.common.CommonMiddleware') + 1,
            'debug_toolbar.middleware.DebugToolbarMiddleware'
        )

if import_app('picker', INSTALLED_APPS):
    PICKER_INSTALLED_APPS = (
        'jquery',
        'bootstrap',
        'less',
    )

WSGI_APPLICATION = 'trip.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

ENGINE = getattr(configs, 'ENGINE', None)
if not ENGINE or ENGINE == 'sqlite3':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': getattr(configs, 'DB_NAME', str(BASE / 'db.sqlite3')),
        }
    }
elif ENGINE == 'mysql':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': getattr(configs, 'DB_NAME', 'dbname'),
            'USER': getattr(configs, 'DB_USER', 'root'),
            'PASSWORD': getattr(configs, 'DB_PASSWORD', 'password'),
            'HOST': getattr(configs, 'DB_HOST', '127.0.0.1'),
        }
    }

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Asia/Bangkok'
USE_I18N = True
USE_L10N = True
USE_TZ = False
DATE_FORMAT = "SHORT_DATE_FORMAT"

STATIC_URL = '/static/'
STATICFILES_DIRS = [str(BASE / 'static')]
STATIC_ROOT = str(BASE / 'static_root')
MEDIA_URL = '/media/'
MEDIA_ROOT = str(BASE / 'media')
