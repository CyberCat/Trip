# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings


def brand(request):
    return {
        'brand': getattr(settings, 'BRAND_NAME', ''),
    }
