from django.contrib import admin
from .models import Rent, Payment

class RentAdmin(admin.ModelAdmin):
	list_display=[f.name for f in Rent._meta.fields]
admin.site.register(Rent, RentAdmin)

class PaymentAdmin(admin.ModelAdmin):
	list_display=[f.name for f in Payment._meta.fields]
admin.site.register(Payment, PaymentAdmin)