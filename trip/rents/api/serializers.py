from ..models import Rent, Payment
from rest_framework import serializers

class RentSerializers(serializers.ModelSerializer):
  class Meta:
    model = Rent
    fields = '__all__'

class PaymentSerializers(serializers.ModelSerializer):
  class Meta:
    model = Payment
    fields = '__all__'
