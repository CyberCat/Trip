from rest_framework import generics, status
from rest_framework.response import Response
from .serializers import (
  RentSerializers, PaymentSerializers
)
from ..models import Rent, Payment

class RentView(generics.ListAPIView):
  permission_classes = []
  model = Rent
  serializer_class = RentSerializers

  def get_queryset(self):
    return self.model.objects.all()

'''
class RentActivityView(generics.ListAPIView):
  permission_classes = []
  model = RentActivity
  serializer_class = RentActivitySerializers
  
  def get_queryset(self):
    return self.model.objects.all()
'''
