from django.conf.urls import url
from . import views

app_name = 'api_inventory'

urlpatterns = [
  url(r'^rentlist', views.RentView.as_view(), name="activitylist"),
]