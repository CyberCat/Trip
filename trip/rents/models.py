from django.db import models
from accounts.models import User

class Rent(models.Model):
  user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
  start_date = models.DateTimeField(auto_now_add=True, blank=True)
  end_date = models.DateTimeField(auto_now_add=True, blank=True)
  total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

class Payment(models.Model):
  INPROGRESS = 'P'
  CANCEL = 'C'
  DONE = 'D'
  ERROR = 'E'
  STATUS_CHOICES = (
    (INPROGRESS, 'In Progress'),
    (CANCEL, 'Cancle'),
    (DONE, 'Done'),
    (ERROR, 'Error'),
  )
  rent = models.ForeignKey(Rent, on_delete=models.CASCADE, blank=False)
  status = models.CharField(max_length=1, choices=STATUS_CHOICES)
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
  trans_date = models.DateTimeField(auto_now_add=True, blank=True)