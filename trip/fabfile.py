from fabric.api import local


def start():
    local('python manage.py runserver 0.0.0.0:8001')


def static():
    local('python manage.py collectstatic')


def migrate():
    local('python manage.py migrate')


def change():
    local('python manage.py makemigrations')
    migrate()


def install():
    migrate()
    local('python manage.py createsuperuser --username root --email root@root.com')


def superuser():
    local('python manage.py createsuperuser --username root --email root@root.com')


def notebook():
    local('python manage.py shell_plus --notebook')


def requirement():
    local('pip install -r requirements.txt')
