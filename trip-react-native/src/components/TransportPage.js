import { BackHandler, Image, ListView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Button, CheckBox } from 'react-native-elements';
import { Drawer, List, Stepper, TabBar } from 'antd-mobile';

import DateTimePicker from 'react-native-modal-datetime-picker'
import InfoActions from '../modules/info/actions';
import InfoSelectors from '../modules/info/selectors';
import Input from 'antd-mobile/lib/input-item/Input';
import Moment from 'moment'
import React from 'react'
import SideMenu from '../layouts/SideMenu'
import TabMenu from '../layouts/TabMenu'
import { compose } from 'redux';
import { connect } from 'react-redux';
import styles from '../static/css/AppStyle'

const titleText = "Transport";

const NUM_SECTIONS = 1;
const NUM_ROWS_PER_SECTION = 1;
let pageIndex = 0;

const dataBlops = {};
let sectionIDs = [];
let rowIDs = [];

const startDate = new Date;
const endDate = new Date;
const myFormat = 'DD-MM-YYYY';

function genData(pIndex = 0){
    for(let i = 0;i < NUM_SECTIONS ; i++){
        const ii = (pIndex * NUM_SECTIONS) + i;
        const sectionName = 'Section ${ii}';
        sectionIDs.push(sectionName);
        dataBlops[sectionName] = sectionName;
    }
}

class TransportPage extends React.Component{
    //เรียกข้อมูลรถ this.props.transportList หรือ const {transportList} = this.props
    constructor(props) {
        super(props);
        this.setBack = this.setBack.bind(this);

        const getSectionData = (dataBlop, sectionID) => dataBlop[sectionID];
        const getRowData = (dataBlop, sectionID, rowID) => dataBlop[rowID];

        const dataSource = new ListView.DataSource({
            getRowData,
            getSectionHeaderData: getSectionData,
            rowHasChanged: (row1, row2) => row1 !== row2,
            sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
        });

        this.state = {
            dataSource,
            isLoading: true,
            noPeople: 1,
            filPrice: 0,
        };
    }
            
    setBack = () => {
        const { goBack } = this.props.navigation;
        goBack();
        return true;
    }

    fetch = () => {
        const { getAllCar } = this.props
        return Promise.all([getAllCar()])
    }

    componentDidMount(){
        this.fetch();
        BackHandler.addEventListener('hardwareBackPress', this.setBack);

        setTimeout(()=>{
            genData();
            this.setState({
                dataSource: this.state.dataSource.cloneWithRowsAndSections(dataBlops,sectionIDs.rowIDs),
                isLoading: false,
            });
        },600);
    }
    
    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.setBack);
    }
    
    static navigationOptions = {
        header: null
    };
    
    state = {
        open: false,
    };

    _startshowDateTimePicker = () => this.setState({ isSDateTimePickerVisible: true });
    _endshowDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
 
    _starthideDateTimePicker = () => this.setState({ isSDateTimePickerVisible: false });
    _endhideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _starthandleDatePicked = (date) => {
      console.log('A date has been picked: ', date);
      startDate = date;
      this._starthideDateTimePicker();
    };

    _endhandleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        endDate = date;
        this._endhideDateTimePicker();
      };

    onClick = () => {
        this.setState({ open: !this.state.open });
    }

    onEndReached = (event) => {
        if(this.state.isLoading && this.state.hasMore){
            return;
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.dataSource !== this.props.dataSource){
            this.setState({
                dataSource: this.state.dataSource.cloneWithRowsAndSections(nextProps.dataSource),
            });
        }
    }

    changeNoPeople = (noPeople) =>{
        this.setState({noPeople})
    }

    changeFilPrice = (filPrice) =>{
        filPrice=filPrice*100
        this.setState({filPrice})
    }

    render(){
        const { navigate } = this.props.navigation;
        const infomation = this.props.transportList;
        function MyBody(props){
            return(
                <View>
                    {props.children}
                </View>
            );
        }

        const data=infomation;

        const sidebar = (<List>
            {[0,1,2].map((i,index)=>{
              if(index === 0){
                  return (<List.Item key={index}>User01</List.Item>);
              }
              if(index === 1){
                  return (<List.Item key={index}>Check history</List.Item>);
              }
              if(index === 2){
                  return (<List.Item key={index}>Logout</List.Item>);
              }
            })}</List>
        );

        const seperator = (sectionID, rowID) =>{
            <View
                key = {'${sectionID}-${rowID}'}
                style={{
                    height:3,
                    backgroundColor: '#F5F5F9',
                }}
            />
        };

        if(data != undefined){
            let index = data.length-1;
            const row = (rowData, sectionID, rowID) => {
                if(index<0){
                    index = data.length-1;
                }
                const obj = data[index--];
                return(
                    <TouchableOpacity onPress={()=>navigate('DetailTran',{titleText: titleText, car_name: obj.carName, img: obj.image, noPeople: this.state.noPeople,
                        start:Moment(startDate).format(myFormat), end:Moment(endDate).format(myFormat), price:obj.price, car_type: obj.carType,
                        car_info: obj.carInfo, driver_name: obj.driverName, driver_info: obj.driverInfo, service: obj.serviceArea})}>
                        <View key={rowID} style={{borderBottomWidth:15, borderColor:"#F5F5F9"}}>
                            <View>
                            <Text style={styles.txtName}>{obj.carName}</Text></View>
                            <View style={styles.picbox}>
                                <Image style={styles.imgStyleList} resizeMode="cover" source={{uri: obj.image}}/>
                                <View>
                                    <View><Text>สีของรถ: {obj.carInfo}</Text></View>
                                    <View><Text>ชนิดของรถ: {obj.carType}</Text></View>
                                    <View><Text>ค่าเช่ารถ: {obj.price}</Text></View>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                );
            };

        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <SideMenu onClick={this.onClick} name={titleText} />
                </View>
                <Drawer
                    className="my-drawer"
                    enableDragHandle
                    style={styles.container}
                    sidebar={sidebar}
                    open={this.state.open}
                    >
                    {/*searching layer*/}
                    <View style={styles.search}>
                        <View style={styles.viewRow}>
                            <TouchableOpacity onPress={this._startshowDateTimePicker}>
                                <Text>วันเริ่มเดินทาง: <Text>{Moment(startDate).format(myFormat)}    </Text></Text>
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isSDateTimePickerVisible}
                                onConfirm={this._starthandleDatePicked}
                                onCancel={this._starthideDateTimePicker}
                            />
                            <TouchableOpacity onPress={this._endshowDateTimePicker}>
                                <Text>วันกลับ: <Text style={styles.txtDate}>{Moment(endDate).format(myFormat)}</Text></Text>
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this._endhandleDatePicked}
                                onCancel={this._endhideDateTimePicker}
                            />
                        </View>
                        <View style={styles.viewRow}>
                            <Text>จำนวนคน: </Text>
                            <Stepper
                                        showNumber
                                        max={10}
                                        min={1}
                                        value={this.state.noPeople}
                                        onChange={this.changeNoPeople}
                                    />  
                            <Text>ค่าเช่ารถ: </Text>
                            <Stepper
                                        showNumber
                                        min={0}
                                        value={this.state.filPrice}
                                        onChange={this.changeFilPrice}
                                    />  
                        </View>
                        <View style={styles.viewRow}>
                            <Text style={styles.textVertical}>ชนิดของรถ</Text>
                            <CheckBox title="รถตู้" containerStyle={styles.checkBoxStyle} textStyle={styles.checkBoxtxtStyle}/>
                            <CheckBox title="รถแท็กซี่" containerStyle={styles.checkBoxStyle} textStyle={styles.checkBoxtxtStyle}/>
                            <CheckBox title="อื่นๆ" containerStyle={styles.checkBoxStyle} textStyle={styles.checkBoxtxtStyle}/>
                        </View>
                        <Button title="ค้นหา" buttonStyle={styles.btnStyle} titleStyle={styles.btnComponent}/>
                    </View>
                    <ListView
                        ref={el=>this.lv = el}
                        dataSource={this.state.dataSource}
                        {...console.log(this.state.dataSource)}
                        renderFooter={()=>(<View>
                            <Text>{this.setState.isLoading ? 'Loading...':'Loaded'}</Text>
                        </View>)}
                        renderBodyComponent={()=><MyBody/>}
                        renderRow={row}
                        renderSeparator={seperator}
                        style={{
                            height: this.state.height,
                        }}
                        pageSize={3}
                        scrollRenderAheadDistance={500}
                        onEndReached={this.onEndReached}
                        onEndReachedThreshold={10}
                    />
                </Drawer>
                <View style={styles.head}> 
                    <TabMenu navigate={navigate} page='Transport'/>
                </View>
            </View>
        );}else{
            return (
                <View style={styles.container}>
                    <View style={styles.head}>
                        <SideMenu onClick={this.onClick} name={titleText} />
                    </View>
                </View>
            )
        }
    }
}

const mapStateToProps = state => ({
    transportList: InfoSelectors.transportList(state)
});
  
const mapDispatchToProps = dispatch => ({
    getAllCar: () => dispatch(InfoActions.getAllCar())
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(TransportPage);   
