import { Drawer, List, TabBar } from 'antd-mobile';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import React from 'react'
import SideMenu from '../layouts/SideMenu'
import TabMenu from '../layouts/TabMenu'
import styles from '../static/css/AppStyle'

const titleText = "Ready Pathum";

class HomePage extends React.Component {
  state = {
    open: false,
  }

  onClick = () => {
    this.setState({ open: !this.state.open });
  }

  static navigationOptions = {
    header: null
  };

  render() {
    //const {titleText} = this.state;
    const { navigate } = this.props.navigation;
    const sidebar = (<List>
      {[0,1,2].map((i,index)=>{
        if(index === 0){
            return (<List.Item key={index}>User01</List.Item>);
        }
        if(index === 1){
            return (<List.Item key={index}>Check history</List.Item>);
        }
        if(index === 2){
            return (<List.Item key={index}>Logout</List.Item>);
        }
      })}</List>
  );

    return (
      <View style={styles.container}>
        <View style={styles.head}>
          <SideMenu onClick={this.onClick} name={titleText} />
        </View>
        <Drawer
          className="my-drawer"
          enableDragHandle
          style={styles.container}
          sidebar={sidebar}
          open={this.state.open}
        >
        <TouchableOpacity style={styles.itemMain} onPress={()=>navigate('Activity',{})}>
          <Image style={styles.imgStyle} source={require('../static/img/activities.png')}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.itemMain} onPress={()=>navigate('Transport',{})}>
          <Image style={styles.imgStyle} source={require('../static/img/transports.png')}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.itemMain} onPress={()=>navigate('Hotel',{})}>
          <Image style={styles.imgStyle} source={require('../static/img/hotel.png')}/>
        </TouchableOpacity>  
        </Drawer>
        <View style={styles.tabBar}> 
          <TabMenu navigate={navigate} page='Home'/>
        </View>
      </View>
    )
  }
}

export default HomePage;
