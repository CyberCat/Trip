import { BackHandler, Image, ListView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Button, CheckBox } from 'react-native-elements';
import { Drawer, List, Modal, TabBar } from 'antd-mobile';

import Input from 'antd-mobile/lib/input-item/Input';
import React from 'react'
import SideMenu from '../layouts/SideMenu'
import TabMenu from '../layouts/TabMenu'
import styles from '../static/css/AppStyle'

const titleText = "Reservation";

const alert = Modal.alert;

function MyBody(props){
    return(
        <View>
            {props.children}
        </View>
    );
}

const data = [
    {
        img: require('../static/img/activities.png'),
        title: 'activites',
        des: '',
    },
    {
        img: require('../static/img/transports.png'),
        title: 'Transport',
        des: '',
    },
];

const NUM_SECTIONS = 5;
const NUM_ROWS_PER_SECTION = 5;
let pageIndex = 0;

const dataBlops = {};
let sectionIDs = [];
let rowIDs = [];

function genData(pIndex = 0){
    for(let i = 0;i < NUM_SECTIONS ; i++){
        const ii = (pIndex * NUM_SECTIONS) + i;
        const sectionName = 'Section ${ii}';
        sectionIDs.push(sectionName);
        dataBlops[sectionName] = sectionName;
        rowIDs[ii] = [];
        
        for(let j = 0; j< NUM_ROWS_PER_SECTION;j++){
            const rowName = 'S${ii}, R${j}';
            rowIDs[ii].push(rowName);
            dataBlops[rowName] = rowName;
        }
    }
    sectionIDs = [...sectionIDs];
    rowIDs = [...rowIDs];
}

class ReservationPage extends React.Component{
    constructor(props) {
        super(props);
        this.setBack = this.setBack.bind(this);

        const getSectionData = (dataBlop, sectionID) => dataBlop[sectionID];
        const getRowData = (dataBlop, sectionID, rowID) => dataBlop[rowID];

        const dataSource = new ListView.DataSource({
            getRowData,
            getSectionHeaderData: getSectionData,
            rowHasChanged: (row1, row2) => row1 !== row2,
            sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
        });

        this.state = {
            dataSource,
            isLoading: true,
           
        };
    }
            
    setBack = () => {
        const { goBack } = this.props.navigation;
        goBack();
        return true;
    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.setBack);

        setTimeout(()=>{
            genData();
            this.setState({
                dataSource: this.state.dataSource.cloneWithRowsAndSections(dataBlops,sectionIDs.rowIDs),
                isLoading: false,
            });
        },600);
    }
    
    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.setBack);
    }
    
    static navigationOptions = {
        header: null
    };
    
    state = {
        open: false,
    };

    onClick = () => {
        this.setState({ open: !this.state.open });
    }

    onEndReached = (event) => {
        if(this.state.isLoading && this.state.hasMore){
            return;
        }
        this.setState({isLoading: true});
        setTimeout(() => {
            genData(++pageIndex)
            this.setState({
                dataSource: this.state.dataSource.cloneWithRowsAndSections(dataBlops,sectionIDs,rowIDs),
                isLoading: false,
            });
        }, 1000);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.dataSource !== this.props.dataSource){
            this.setState({
                dataSource: this.state.dataSource.cloneWithRowsAndSections(nextProps.dataSource),
            });
        }
    }

    render(){
        const { navigate } = this.props.navigation;

        const sidebar = (<List>
            {[0,1,2].map((i,index)=>{
              if(index === 0){
                  return (<List.Item key={index}>User01</List.Item>);
              }
              if(index === 1){
                  return (<List.Item key={index}>Check history</List.Item>);
              }
              if(index === 2){
                  return (<List.Item key={index}>Logout</List.Item>);
              }
            })}</List>
        );

        const seperator = (sectionID, rowID) =>{
            <View
                key = {'${sectionID}-${rowID}'}
                style={{
                    height:3,
                    backgroundColor: '#F5F5F9',
                }}
            />
        };

        let index = data.length-1;
        const row = (rowData, sectionID, rowID) => {
            if(index<0){
                index = data.length-1;
            }
            const obj = data[index--];
            return(
                <View key={rowID} style={{borderBottomWidth:15, borderColor:"#F5F5F9"}}>
                    <View>
                        <Text style={styles.txtName}>{obj.title}</Text>
                    </View>
                    <View>
                        <View><Text>{obj.des}</Text></View>
                        <View><Text>100$ {rowID}</Text></View>
                    </View>
                </View>
            );
        };
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <SideMenu onClick={this.onClick} name={titleText} />
                </View>
                <Drawer
                    className="my-drawer"
                    enableDragHandle
                    style={styles.container}
                    sidebar={sidebar}
                    open={this.state.open}
                    >
                    <Text>Activities ที่ทำการจอง</Text>
                    <ListView
                        ref={el=>this.lv = el}
                        dataSource={this.state.dataSource}
                        {...console.log(this.state.dataSource)}
                        renderFooter={()=>(<View>
                            <Text>{this.setState.isLoading ? 'Loading...':'Loaded'}</Text>
                        </View>)}
                        renderBodyComponent={()=><MyBody/>}
                        renderRow={row}
                        renderSeparator={seperator}
                        style={{
                            height: this.state.height,
                        }}
                        pageSize={3}
                        scrollRenderAheadDistance={500}
                        onEndReached={this.onEndReached}
                        onEndReachedThreshold={10}
                    />
                </Drawer>
                <Button title='กดเพื่อจ่ายเงิน' 
                    onPress={()=> alert('ยอดเงินในบัญชี: 100','ท่านต้องการจ่ายเงินเพื่อยืนยันการจองข้อมูลดังกล่าวหรือไม่ หากกดใช่ ระบบจะทำการตัดเงินในบัญชีของท่าน (กรุณาตรวจสอบว่าบัญชีมีเงินเพียงพอ)', [
                    { text: 'ยกเลิก', onPress: () => console.log('ยกเลิก') },
                    { text: 'ยืนยัน', onPress: () => navigate('Payment',{}) },
                ])}></Button>
                <View style={styles.tabBar}> 
                    <TabMenu navigate={navigate} page='Reservation'/>
                </View>
            </View>
        );
    }
}

export default ReservationPage;   
