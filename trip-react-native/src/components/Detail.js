import { Image, ListView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { List, TabBar } from 'antd-mobile';

import { Button } from 'react-native-elements';
import ModalMenu from '../layouts/ModalMenu'
import React from 'react'
import SideMenu from '../layouts/SideMenu'
import TabMenu from '../layouts/TabMenu'
import styles from '../static/css/AppStyle'

class Detail extends React.Component {
    static navigationOptions = {
        header: null
    };

    state = {
        open: false,
    };

    onClick = () => {
        this.setState({ open: !this.state.open });
    }

    onEndReached = (event) => {
        if(this.state.isLoading && this.state.hasMore){
            return;
        }
        this.setState({isLoading: true});
        setTimeout(() => {
            genData(++pageIndex)
            this.setState({
                dataSource: this.state.dataSource.cloneWithRowsAndSections(dataBlops,sectionIDs,rowIDs),
                isLoading: false,
            });
        }, 1000);
    }
    
    render() {
        const {state, navigate} = this.props.navigation
        console.log(state.params.titleText)

        const sidebar = (<List>
            {[0,1,2].map((i,index)=>{
              if(index === 0){
                  return (<List.Item key={index}>User01</List.Item>);
              }
              if(index === 1){
                  return (<List.Item key={index}>Check history</List.Item>);
              }
              if(index === 2){
                  return (<List.Item key={index}>Logout</List.Item>);
              }
            })}</List>
        );

        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <SideMenu onClick={this.onClick} name={state.params.titleText} />
                </View>
                <View style={styles.headDetail}>
                    <View style = {styles.headDetailLeft}>
                        <Text style={styles.txtDetailHead} numberOfLines={1} ellipsizeMode={'tail'}>{state.params.title}</Text>
                        <Text style={styles.txtSubDetail}>จำนวนคน: {state.params.noPeople}</Text>
                        <Text style={styles.txtSubDetail}>วันเดินทาง: {state.params.start}</Text>
                        <Text style={styles.txtSubDetail}>วันกลับ: {state.params.end}</Text>
                        <Text>ค่าใช้จ่าย: {state.params.price} ฿</Text>
                    </View>
                    <ModalMenu state={state} page='Hotel' navigate={navigate} />
                </View>
                <View style={styles.bodyDetail}>
                    <ScrollView onContentSizeChange={(width, height) => {console.log(width, height);}}>
                        <View style={{paddingBottom: '50%'}}>
                            <Text>{state.params.title}</Text>
                            <View style={styles.imgBox}>
                                <Image style={styles.imgStyle} resizeMode="contain" resizeMode="cover" source={{uri: state.params.img}}/>
                            </View>
                            <Text>สถานที่: {state.params.locate}</Text>
                            <Text>ข้อมูล: {state.params.info}</Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.tabBar}> 
                    <TabMenu navigate={navigate} />
                </View>
            </View>
        )
    }
}

export default Detail;
