import { Drawer, List, TabBar } from 'antd-mobile';
import { Image, ListView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { Button } from 'react-native-elements';
import ModalMenu from '../layouts/ModalMenu'
import React from 'react'
import SideMenu from '../layouts/SideMenu'
import TabMenu from '../layouts/TabMenu'
import styles from '../static/css/AppStyle'

const titleText = 'Payment'

class PaymentPage extends React.Component {

    static navigationOptions = {
        header: null
    };

    state = {
        open: false,
    };

    onClick = () => {
        this.setState({ open: !this.state.open });
    }

    render() {
        const { navigate } = this.props.navigation;

        const sidebar = (<List>
            {[0,1,2].map((i,index)=>{
              if(index === 0){
                  return (<List.Item key={index}>User01</List.Item>);
              }
              if(index === 1){
                  return (<List.Item key={index}>Check history</List.Item>);
              }
              if(index === 2){
                  return (<List.Item key={index}>Logout</List.Item>);
              }
            })}</List>
        );
        
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <SideMenu onClick={this.onClick} name={titleText} />
                </View>
                <Drawer
                    className="my-drawer"
                    enableDragHandle
                    style={styles.container}
                    sidebar={sidebar}
                    open={this.state.open}
                    >
                    <View style={{margin:20}}>
                        {/*Text*/}
                        <Text style={{textAlign:'center', fontSize:17}}>แสดง QR Code นี้ต่อเจ้าหน้าที่ประจำสถานที่กิจกรรม/รถ/โรงแรม เพื่อยืนยันการเข้าพัก</Text>
                    </View>
                    <View style={{height:'60%', margin:10}}>
                        {/*QR Code*/}
                        <Image style={styles.imgStyle} resizeMode="contain" source={require('../static/img/frame.png')}/>
                    </View>
                </Drawer>
                <View style={styles.tabBar}> 
                    <TabMenu navigate={navigate} page=''/>
                </View>
            </View>
        )
    }
}

export default PaymentPage;
