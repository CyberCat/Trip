import {Text, View} from 'react-native';

import { Button } from 'react-native-elements';
import { Modal } from 'antd-mobile';
import React from 'react';
import styles from '../static/css/AppStyle'

const alert = Modal.alert;

class ModalMenu extends React.Component {

    render() {
        const { navigate } = this.props;
        const {state, page} = this.props
        return (
            <View style={styles.headDeatilRight}>
                <Button 
                    buttonStyle={styles.btnInDetail} 
                    textStyle={styles.txtInBtnStyle} 
                    title="จอง" 
                    onPress={()=> alert('กรุณายืนยันการจอง', 
                        state.params.title, [
                        { text: 'ยกเลิก', onPress: () => console.log('cancel') },
                        { text: 'ยืนยัน', onPress: () => navigate(page) },
                    ])}
                />
            </View>
        );
    }
}

export default ModalMenu