import {PickerView, TabBar} from 'antd-mobile';
import { TabNavigator, TabView } from 'react-navigation'; // 2.0.1
import { Text, View } from 'react-native';

import { Icon } from 'react-native-elements';
import React from 'react'

class TabMenu extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            fullscreen: false,
            home:'Home',
            activity:"Activities",
            transport:"Transports",
            hotel:"Hostel",
            resum:"Reservation",
            selectedTab: this.props.page,
            hidden: false,
        };
    }

    renderContent(pageText) {
        return (
            <View
                onClick={(e) => {
                    e.preventDefault();
                        this.setState({
                            hidden: !this.state.hidden,
                        });
                }}
            />
        );
    }

    render() {
        const {home, activity, transport, hotel, resum} = this.state;
        const { navigate } = this.props;
        return (
          <TabBar
            unselectedTintColor="#949494"
            tintColor="#33A3F4"
            barTintColor="white"
            hidden={this.state.hidden}
          >
            <TabBar.Item
              title={home}
              key={home}
              icon={require('../static/img/home.png')}
              //selectedIcon={}
              selected={this.state.selectedTab === 'Home'}
              onPress={() => navigate('Home',{})}
            >
                {this.renderContent({home})}
            </TabBar.Item>
            <TabBar.Item
              icon={require('../static/img/activity.png')}
              //selectedIcon={}
              title={activity}
              key={activity}
              selected={this.state.selectedTab === 'Activity'}
              onPress={() => navigate('Activity',{})}
            >
              {this.renderContent({activity})}
            </TabBar.Item>
            <TabBar.Item
              icon={require('../static/img/car.png')}
              //selectedIcon={}
              title={transport}
              key={transport}
              dot
              selected={this.state.selectedTab === 'Transport'}
              onPress={() => navigate('Transport',{})}
            >
              {this.renderContent({transport})}
            </TabBar.Item>
            <TabBar.Item
              icon={require('../static/img/sleep.png')}
              //selectedIcon={}
              title={hotel}
              key={hotel}
              dot
              selected={this.state.selectedTab === 'Hotel'}
              onPress={() => navigate('Hotel',{})}
            >
              {this.renderContent({hotel})}
            </TabBar.Item>
            <TabBar.Item
              icon={require('../static/img/reservation.png')}
              //selectedIcon={}
              title={resum}
              key={resum}
              dot
              selected={this.state.selectedTab === 'Reservation'}
              onPress={() => navigate('Reservation',{})}
            >
              {this.renderContent({resum})}
            </TabBar.Item>
          </TabBar>
        );
      }
    }
/*
    render(){
        const {home, activity, transport, hotel, resum} = this.state;
        //const {s} = this.props;
        return (
            <TabBar unselectedTintColor="#949494" tintColor="#33A3F4" barTintColor="white" hidden={true}>
                <TabBar.Item title={home} key="home" ></TabBar.Item>
                <TabBar.Item title={activity} key="activity" ></TabBar.Item>
                <TabBar.Item title={transport} key="transport" ></TabBar.Item>
                <TabBar.Item title={hotel} key="hotel" ></TabBar.Item>
                <TabBar.Item title={resum} key="resum" ></TabBar.Item>
            </TabBar>
        );
    }
}
*/
export default TabMenu;