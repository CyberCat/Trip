import {Drawer, Flex, List, NavBar} from 'antd-mobile';
import { Image, StyleSheet, Text, View } from 'react-native';

import { Icon } from 'react-native-elements';
import React from 'react';

class SideMenu extends React.Component{

    render() {
        const {name, onClick} = this.props;
        return ( //return not login
            <View style={{backgroundColor: 'black', flex:1}}>
                <Flex wrap="wrap">
                <Icon 
                    style={{ zIndex: 5, position: 'absolute'}}
                    name='menu'
                    type='feather'
                    size={45}
                    containerStyle={{top: '3%', left: '10%'}}
                    color='#FFFFFF'
                    onPress={onClick}
                />
                </Flex>
                <Text style={{fontSize: 40, fontWeight: 'bold', color: 'white', textAlign: 'center', position: 'absolute', width: '80%', top: '5%', left: '12%', zIndex: 0}}>{name}</Text>
            </View>
        );
    }
}

export default SideMenu;