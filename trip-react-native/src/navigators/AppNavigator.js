import ActivitiesPage from '../components/ActivitiesPage'
import Detail from '../components/Detail'
import HomePage from '../components/HomePage'
import HotelPage from '../components/HotelPage';
import PropTypes from 'prop-types'
import React from 'react'
import ReservationPage from '../components/ReservationPage'
import PaymentPage from '../components/PaymentPage'
import DetailTran from '../components/DetailTran'
import DetailAc from '../components/DetailAc'
import { StackNavigator } from 'react-navigation'
import TransportPage from '../components/TransportPage';
import { addListener } from '../utils/redux'
import { connect } from 'react-redux'

//import LoginPage from '../components/LoginPage';
export const AppNavigator = StackNavigator({
  Home: { screen: HomePage },
  Activity: { screen: ActivitiesPage },
  Transport: { screen: TransportPage },
  Hotel: { screen: HotelPage },
  Detail: { screen: Detail },
  Reservation: { screen: ReservationPage },
  Payment: { screen: PaymentPage },
  DetailTran: { screen: DetailTran },
  DetailAc: { screen: DetailAc }
})

class AppWithNavigationState extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired
  }

  render() {
    const { dispatch, nav } = this.props
    return (
      <AppNavigator
        navigation={{
          dispatch: dispatch,
          state: nav,
          addListener
        }}
      />
    )
  }
}

const mapStateToProps = state => ({
  nav: state.nav
  //me: LoginSelectors.me(state),
})

export default connect(mapStateToProps)(AppWithNavigationState)
