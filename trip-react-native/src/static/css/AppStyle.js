import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    head:{
        width: '100%',
        height: '11%',
    },
    tabBar:{
        width: '100%',
        height: '8%',
    },
    container:{
        padding: 2,
        backgroundColor: 'white',
        flex: 1,
    },
    search:{
        width: '100%',
        backgroundColor:'white',
        borderWidth: 2,
        marginTop: 3,
        marginBottom: 0,
        flexDirection:'column',
        padding:10,
    },
    list:{
        width: '100%',
        height: '25%',
        backgroundColor:'white',
        borderWidth: 2,
    },
    imgStyle:{
       width: "100%",
       height: "100%",
    },
    imgStyleList:{
        height: '95%'
    },
    picbox:{
        flex: 1,
        height: 200,
        marginBottom: 50,
    },
    checkBoxStyle:{
        backgroundColor:"white",
        padding:0, 
        borderWidth:0, 
        marginRight:0,
    },
    inputStyle:{
        height:20, 
        padding:0,
        width:"25%", 
        borderWidth:1, 
        marginRight:10,
        marginBottom:5
    },
    txtDate:{
        width:'25%'
    },
    viewRow:{
        flexDirection: 'row',
    },
    textVertical:{
        textAlignVertical:'center'
    },
    checkBoxtxtStyle:{
        fontWeight:'normal', 
        fontSize:13
    },
    txtName:{
        fontSize:16,
    },
    btnStyle:{
        padding:0,
    },
    btnComponent:{
        fontSize:14,
    },
    itemMain:{
        height: '33%',
        width: '100%',
        borderWidth: 2,
        borderColor:'white',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headDetail:{
        flexDirection: 'row',
        height:'20%',
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
    },
    headDetailLeft:{
        flexDirection: 'column',
        width: '60%',
    },
    headDeatilRight:{
        width:'40%',
        flex:1,
        justifyContent:'center',
        marginTop:'4.5%'
    },
    txtDetailHead:{
        fontSize:24,
        fontWeight: 'bold',
    },
    btnInDetail:{
        width: '100%',
        height: '80%',
    },
    txtInBtnStyle:{
        fontSize: 24
    },
    txtSubDetail:{
        fontSize: 14,
        marginRight: 10,
        marginBottom: 5,
    },
    bodyDetail:{
        marginTop: 15,
        marginLeft: 20,
        marginRight: 20,
        borderTopWidth:1,
        flex:1
    },
    imgBox:{
        marginTop:10,
        marginBottom: 10,
        height: '50%'
    },
    headInDetail:{
        fontSize: 18,
    }
});