import { List } from 'antd-mobile';
import React from 'react';

SideBar = (loginStatus) => {
  if(loginStatus){
    return (
      <List>
        {[0,1,2].map((i,index)=>{
        if(index === 0){
            return (<List.Item key={index}>Login</List.Item>);
        }
        if(index === 1){
            return (<List.Item key={index}>Login via facebook</List.Item>)
        }
        if(index === 2){
            return (<List.Item key={index}>Login via google</List.Item>)
        }
      })}</List>
    )
  }
  return (
    <List>
      {[0,1,2].map((i,index)=>{
      if(index === 0){
          return (<List.Item key={index}>Login</List.Item>);
      }
      if(index === 1){
          return (<List.Item key={index}>Login via facebook</List.Item>)
      }
      if(index === 2){
          return (<List.Item key={index}>Login via google</List.Item>)
      }
    })}</List>
  )
};

export default SideBar;