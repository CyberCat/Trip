import { FacebookLogin } from 'react-facebook-login-component'
import React from 'react'

export class login extends React.Component {
  constructor(props, context) {
    super(props, context)
  }

  responseFacebook(response) {
    console.log(response)
  }

  render() {
    return (
      <div>
        <FacebookLogin
          socialId="2074725169407022"
          language="th"
          scope="public_profile,email"
          responseHandler={this.responseFacebook}
          xfbml={true}
          fields="id,email,name"
          version="v2.5"
          className="facebook-login"
          buttonText="Login With Facebook"
        />
      </div>
    )
  }
}
